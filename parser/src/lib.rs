use nom::{bytes, character, character::streaming::char, IResult, *};

fn word(i: &str) -> IResult<&str, &str> {
    bytes::streaming::take_while1(|c: char| c.is_alphanumeric())(i)
}

#[derive(Debug, PartialEq, Eq)]
pub enum Part {
    Literal(String),
    Scalar(String),
    Array(String),
}

fn array(i: &str) -> IResult<&str, String> {
    combinator::map(
        sequence::preceded(
            char('@'),
            branch::alt((word, sequence::delimited(char('{'), word, char('}')))),
        ),
        ToString::to_string,
    )(i)
}

fn scalar(i: &str) -> IResult<&str, String> {
    combinator::map(
        sequence::preceded(
            char('$'),
            branch::alt((word, sequence::delimited(char('{'), word, char('}')))),
        ),
        ToString::to_string,
    )(i)
}

fn literal(i: &str) -> IResult<&str, String> {
    combinator::verify(
        bytes::streaming::escaped_transform(
            bytes::streaming::take_while1(|c| c != '\\' && c != '"' && c != '@' && c != '$'),
            '\\',
            branch::alt((
                combinator::map(bytes::streaming::tag("\\"), |_| "\\"),
                combinator::map(bytes::streaming::tag("\""), |_| "\""),
                combinator::map(bytes::streaming::tag("n"), |_| "\n"),
                combinator::map(bytes::streaming::tag("e"), |_| "\x1b"),
                combinator::map(bytes::streaming::tag("$"), |_| "$"),
                combinator::map(bytes::streaming::tag("@"), |_| "$"),
            )),
        ),
        |s: &str| !s.is_empty(),
    )(i)
}

fn double_quoted(i: &str) -> IResult<&str, Vec<Part>> {
    sequence::delimited(
        char('"'),
        multi::many0(branch::alt((
            combinator::map(scalar, Part::Scalar),
            combinator::map(array, Part::Array),
            combinator::map(literal, Part::Literal),
        ))),
        char('"'),
    )(i)
}

fn single_quoted(i: &str) -> IResult<&str, String> {
    sequence::delimited(
        char('\''),
        bytes::streaming::escaped_transform(
            bytes::streaming::take_while1(|c| c != '\'' && c != '\\'),
            '\\',
            |i: &str| {
                branch::alt((
                    combinator::map(bytes::streaming::tag("\'"), |_| "\'"),
                    combinator::map(bytes::streaming::tag("\\"), |_| "\\"),
                    combinator::map(bytes::streaming::tag("n"), |_| "\n"),
                    combinator::map(bytes::streaming::tag("e"), |_| "\x1b"),
                ))(i)
            },
        ),
        char('\''),
    )(i)
}

fn arg(i: &str) -> IResult<&str, Vec<Part>> {
    branch::alt((
        combinator::map(single_quoted, |s| vec![Part::Literal(s)]),
        combinator::map(word, |s| vec![Part::Literal(s.to_string())]),
        combinator::map(array, |s| vec![Part::Array(s)]),
        combinator::map(scalar, |s| vec![Part::Scalar(s)]),
        double_quoted,
    ))(i)
}

fn seq(i: &str) -> IResult<&str, Vec<Vec<Part>>> {
    multi::separated_nonempty_list(character::streaming::space1, arg)(i)
}

pub fn parse(i: &str) -> IResult<&str, Vec<Vec<Part>>> {
    sequence::terminated(
        seq,
        combinator::opt(branch::alt((
            character::streaming::char(';'),
            character::streaming::char('\n'),
        ))),
    )(i)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
