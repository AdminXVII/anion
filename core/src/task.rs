use std::future::Future;
use std::marker::Unpin;
use std::pin::Pin;
use std::task::{Context, Poll};

use super::{
    pipes::{PipeGenerator, PipeIn, PipeOut},
    Accumulator, Job, JobWait,
};

/// A single task (job pipeline)
///
/// Jobs chained one after the other. This is the primary interface of
/// this crate, the equivalent of a unix process group
#[derive(Debug)]
pub struct Task {
    stdin: PipeIn,
    jobs: Vec<Job>,
    stdout: PipeOut,
}

impl<T: Accumulator + Unpin> Future for TaskFuture<T> {
    type Output = std::io::Result<T::Output>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();
        for (i, mut e) in this.jobs.drain(..) {
            match Future::poll(Pin::new(&mut e), ctx) {
                Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                Poll::Ready(Ok(r)) => this.acc.add(i, r)?,
                Poll::Pending => this.temp.push((i, e)),
            }
        }
        std::mem::swap(&mut this.jobs, &mut this.temp);
        if this.jobs.is_empty() {
            Poll::Ready(Ok(this.acc.collect()))
        } else {
            Poll::Pending
        }
    }
}

#[derive(Debug)]
pub struct TaskFuture<T: Accumulator> {
    acc: T,
    /// Avoid allocations by simply moving value between those two vectors
    /// Switch to drain_filter/drain_where when it gets stabilized
    temp: Vec<(usize, JobWait)>,
    jobs: Vec<(usize, JobWait)>,
}

impl Task {
    #[must_use]
    pub fn new(stdin: PipeIn, jobs: Vec<Job>, stdout: PipeOut) -> Self {
        Self {
            jobs,
            stdin,
            stdout,
        }
    }

    pub fn num_jobs(&self) -> usize {
        self.jobs.len()
    }

    pub fn spawn<T: Accumulator>(self) -> std::io::Result<TaskFuture<T>> {
        let pipe_len = self.jobs.len();
        let acc = T::new(pipe_len);

        let jobs = self
            .jobs
            .into_iter()
            .zip(PipeGenerator::new(pipe_len, self.stdin, self.stdout))
            .map(|(j, p)| {
                let p = p?;
                j.spawn(p.0, p.1, PipeOut::Pipe(os_pipe::dup_stderr()?))
            })
            .enumerate()
            .map(|(i, elem)| elem.map(|e| (i, e)))
            .collect::<Result<Vec<_>, _>>()?;
        Ok(TaskFuture {
            jobs,
            acc,
            temp: Vec::with_capacity(pipe_len),
        })
    }
}
