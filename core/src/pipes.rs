use os_pipe::{PipeReader, PipeWriter};
use std::fs::File;
use std::io;
use std::os::unix::io::{FromRawFd, IntoRawFd};
use std::pin::Pin;
use std::process::Stdio;
use std::task::{Context, Poll};

use tokio::io::{AsyncRead, AsyncWrite};

#[derive(Debug)]
pub enum PipeIn {
    File(File),
    Pipe(PipeReader),
    Void,
}

#[derive(Debug)]
enum AsyncPipeInInner {
    File(tokio::fs::File),
    Pipe(tokio::fs::File),
    Void(tokio::io::Empty),
}
pub struct AsyncPipeIn(AsyncPipeInInner);

impl PipeIn {
    pub fn dup_stdin() -> io::Result<Self> {
        os_pipe::dup_stdin().map(Self::Pipe)
    }
}

impl From<PipeIn> for AsyncPipeIn {
    fn from(p: PipeIn) -> Self {
        match p {
            PipeIn::File(f) => Self(AsyncPipeInInner::File(f.into())),
            PipeIn::Pipe(p) => {
                let pipe_file = unsafe { File::from_raw_fd(p.into_raw_fd()) };
                Self(AsyncPipeInInner::Pipe(pipe_file.into()))
            }
            PipeIn::Void => Self(AsyncPipeInInner::Void(tokio::io::empty())),
        }
    }
}

impl AsyncRead for AsyncPipeIn {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<Result<usize, io::Error>> {
        match &mut self.0 {
            AsyncPipeInInner::File(f) => AsyncRead::poll_read(Pin::new(f), cx, buf),
            AsyncPipeInInner::Pipe(p) => AsyncRead::poll_read(Pin::new(p), cx, buf),
            AsyncPipeInInner::Void(v) => AsyncRead::poll_read(Pin::new(v), cx, buf),
        }
    }
}

impl From<PipeIn> for Stdio {
    fn from(p: PipeIn) -> Stdio {
        match p {
            PipeIn::File(f) => f.into(),
            PipeIn::Pipe(p) => p.into(),
            PipeIn::Void => Stdio::null(),
        }
    }
}

#[derive(Debug)]
pub enum PipeOut {
    File(File),
    Pipe(PipeWriter),
    Void,
}

#[derive(Debug)]
enum AsyncPipeOutInner {
    File(tokio::fs::File),
    Pipe(tokio::fs::File),
    Void(tokio::io::Sink),
}
pub struct AsyncPipeOut(AsyncPipeOutInner);

impl PipeOut {
    pub fn dup_stdout() -> io::Result<Self> {
        os_pipe::dup_stdout().map(Self::Pipe)
    }

    pub fn dup_stderr() -> io::Result<Self> {
        os_pipe::dup_stderr().map(Self::Pipe)
    }
}

impl From<PipeOut> for AsyncPipeOut {
    fn from(p: PipeOut) -> Self {
        match p {
            PipeOut::File(f) => Self(AsyncPipeOutInner::File(f.into())),
            PipeOut::Pipe(p) => {
                let pipe_file = unsafe { File::from_raw_fd(p.into_raw_fd()) };
                Self(AsyncPipeOutInner::Pipe(pipe_file.into()))
            }
            PipeOut::Void => Self(AsyncPipeOutInner::Void(tokio::io::sink())),
        }
    }
}

impl AsyncWrite for AsyncPipeOut {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &[u8],
    ) -> Poll<Result<usize, io::Error>> {
        match &mut self.0 {
            AsyncPipeOutInner::File(f) => AsyncWrite::poll_write(Pin::new(f), cx, buf),
            AsyncPipeOutInner::Pipe(p) => AsyncWrite::poll_write(Pin::new(p), cx, buf),
            AsyncPipeOutInner::Void(v) => AsyncWrite::poll_write(Pin::new(v), cx, buf),
        }
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), io::Error>> {
        match &mut self.0 {
            AsyncPipeOutInner::File(f) => AsyncWrite::poll_flush(Pin::new(f), cx),
            AsyncPipeOutInner::Pipe(p) => AsyncWrite::poll_flush(Pin::new(p), cx),
            AsyncPipeOutInner::Void(v) => AsyncWrite::poll_flush(Pin::new(v), cx),
        }
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), io::Error>> {
        match &mut self.0 {
            AsyncPipeOutInner::File(f) => AsyncWrite::poll_shutdown(Pin::new(f), cx),
            AsyncPipeOutInner::Pipe(p) => AsyncWrite::poll_shutdown(Pin::new(p), cx),
            AsyncPipeOutInner::Void(v) => AsyncWrite::poll_shutdown(Pin::new(v), cx),
        }
    }
}

impl From<PipeOut> for Stdio {
    fn from(p: PipeOut) -> Stdio {
        match p {
            PipeOut::File(f) => f.into(),
            PipeOut::Pipe(p) => p.into(),
            PipeOut::Void => Stdio::null(),
        }
    }
}

#[derive(Debug)]
pub struct PipeGenerator {
    pipe_left: usize,
    stdin: Option<PipeIn>,
    stdout: Option<PipeOut>,
}

impl Iterator for PipeGenerator {
    type Item = std::io::Result<(PipeIn, PipeOut)>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pipe_left > 0 {
            let (r, w) = match os_pipe::pipe() {
                Err(e) => return Some(Err(e)),
                Ok(r) => r,
            };
            self.pipe_left -= 1;
            Some(Ok((
                self.stdin.replace(PipeIn::Pipe(r)).unwrap(),
                PipeOut::Pipe(w),
            )))
        } else if let Some(out) = self.stdout.take() {
            Some(Ok((self.stdin.take().unwrap(), out)))
        } else {
            None
        }
    }
}

impl PipeGenerator {
    pub fn new(num: usize, stdin: PipeIn, stdout: PipeOut) -> Self {
        Self {
            pipe_left: num - 1,
            stdin: Some(stdin),
            stdout: Some(stdout),
        }
    }
}
