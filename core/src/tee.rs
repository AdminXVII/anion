use crate::ready;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::{self, AsyncWrite};

#[derive(Debug, Default)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub struct Tee<W: AsyncWrite, O: AsyncWrite> {
    copy: Vec<W>,
    out: Option<O>,
    flushed: usize,
    write: usize,
    bytes: usize,
    shutdown: usize,
}

impl<W: AsyncWrite, O: AsyncWrite> Tee<W, O> {
    pub fn new(copy: Vec<W>, out: Option<O>) -> Self {
        Self {
            copy,
            out,
            flushed: 0,
            write: 0,
            bytes: 0,
            shutdown: 0,
        }
    }
}

impl<W: AsyncWrite + Unpin, O: AsyncWrite + Unpin> AsyncWrite for Tee<W, O> {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        let me = &mut *self;
        while me.write < me.copy.len() {
            me.bytes += ready!(Pin::new(&mut me.copy[me.write]).poll_write(cx, buf))?;
            if me.bytes == buf.len() {
                me.write += 1;
                me.bytes = 0;
            }
        }
        if let Some(mut out) = me.out.as_mut() {
            while me.bytes < buf.len() {
                me.bytes += ready!(Pin::new(&mut out).poll_write(cx, buf))?;
            }
        }
        me.write = 0;
        Poll::Ready(Ok(std::mem::replace(&mut me.bytes, 0)))
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<io::Result<()>> {
        let me = &mut *self;
        while me.flushed < me.copy.len() {
            ready!(Pin::new(&mut me.copy[me.flushed]).poll_flush(cx))?;
            me.flushed += 1;
        }
        if let Some(out) = me.out.as_mut() {
            ready!(Pin::new(out).poll_flush(cx))?;
        }
        me.flushed = 0;
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<io::Result<()>> {
        let me = &mut *self;
        while me.shutdown < me.copy.len() {
            ready!(Pin::new(&mut me.copy[me.shutdown]).poll_flush(cx))?;
            me.shutdown += 1;
        }
        if let Some(out) = me.out.as_mut() {
            ready!(Pin::new(out).poll_flush(cx))?;
        }
        me.shutdown = 0;
        Poll::Ready(Ok(()))
    }
}
