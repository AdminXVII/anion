use std::fmt;
use std::future::Future;
use std::pin::Pin;
use std::process::ExitStatus;
use std::sync::Arc;
use std::task::{Context, Poll};

use tokio::process::{Child, Command};

mod accumulator;
pub mod tee;
#[macro_use]
mod macros;
pub mod pipes;
mod task;

pub use accumulator::{Accumulator, CollectingAccumulator};
pub use os_pipe::PipeReader;
pub use task::Task;
pub use tokio as runtime;

use pipes::{AsyncPipeIn, AsyncPipeOut, PipeIn, PipeOut};

type InternalJobFuture = Pin<Box<dyn Future<Output = std::io::Result<ExitStatus>>>>;

pub enum JobType {
    External(String),
    Func(
        &'static str,
        fn(&[String], AsyncPipeIn, AsyncPipeOut, AsyncPipeOut) -> InternalJobFuture,
    ),
    Closure(
        &'static str,
        Arc<
            dyn Fn(&[String], AsyncPipeIn, AsyncPipeOut, AsyncPipeOut) -> InternalJobFuture
                + Send
                + Sync,
        >,
    ),
}

pub enum JobWait {
    External(Child),
    Internal(InternalJobFuture),
}

#[derive(Debug)]
pub struct Job {
    pub kind: JobType,
    args: Vec<String>,
}

impl fmt::Debug for JobType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::External(name) => writeln!(f, "{:?}", name),
            Self::Closure(name, _) | Self::Func(name, _) => writeln!(f, "{:?} (builtin)", name),
        }
    }
}

impl Future for JobWait {
    type Output = std::io::Result<ExitStatus>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        match &mut *self {
            Self::External(chld) => Future::poll(Pin::new(chld), ctx),
            Self::Internal(chld) => Future::poll(Pin::new(chld), ctx),
        }
    }
}

impl Job {
    #[must_use]
    pub fn new(kind: JobType, args: Vec<String>) -> Self {
        Self { kind, args }
    }

    pub fn spawn(
        self,
        stdin: PipeIn,
        stdout: PipeOut,
        stderr: PipeOut,
    ) -> std::io::Result<JobWait> {
        match self.kind {
            JobType::External(name) => {
                let mut cmd = Command::new(name);
                cmd.kill_on_drop(true);
                cmd.stdin(stdin);
                cmd.stdout(stdout);
                cmd.stderr(stderr);
                cmd.args(self.args);
                Ok(JobWait::External(cmd.spawn()?))
            }
            JobType::Closure(_, func) => Ok(JobWait::Internal(func(
                &self.args,
                stdin.into(),
                stdout.into(),
                stderr.into(),
            ))),
            JobType::Func(_, func) => Ok(JobWait::Internal(func(
                &self.args,
                stdin.into(),
                stdout.into(),
                stderr.into(),
            ))),
        }
    }
}

impl fmt::Debug for JobWait {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::External(name) => writeln!(f, "{:?}", name),
            Self::Internal(..) => writeln!(f, "builtin"),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
