use std::process::ExitStatus;

pub trait Accumulator {
    type Output;

    fn new(len: usize) -> Self;
    fn add(&mut self, index: usize, status: ExitStatus) -> std::io::Result<()>;
    fn collect(&mut self) -> Self::Output;
}

pub struct CollectingAccumulator(Vec<Option<ExitStatus>>);

impl Accumulator for CollectingAccumulator {
    type Output = Vec<ExitStatus>;

    #[must_use]
    fn new(len: usize) -> Self {
        Self(vec![None; len])
    }

    fn add(&mut self, index: usize, status: ExitStatus) -> std::io::Result<()> {
        self.0[index] = Some(status);
        Ok(())
    }

    fn collect(&mut self) -> Self::Output {
        self.0
            .iter_mut()
            .map(|status| status.take().expect("Called poll on a terminated future"))
            .collect()
    }
}
