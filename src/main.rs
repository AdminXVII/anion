use std::io::{self, BufRead, BufReader};
use std::os::unix::process::ExitStatusExt;
use std::process::ExitStatus;

use anion_core::{
    pipes::{PipeIn, PipeOut},
    tee::Tee,
    CollectingAccumulator, Job, JobType, Task,
};
use anion_parser::parse;

fn main() {
    let ret = anion_core::runtime::runtime::Runtime::new()
        .unwrap()
        .block_on(async {
            let stdout = PipeOut::dup_stdout()?;
            let stdin = PipeIn::dup_stdin()?;
            Task::new(
                stdin,
                vec![
                    Job::new(
                        JobType::External("printf".to_string()),
                        vec!["bob".to_string()],
                    ),
                    Job::new(
                        JobType::Func("tee", |_args, mut stdin, stdout, _stderr| {
                            Box::pin(async move {
                                tokio::io::copy(
                                    &mut stdin,
                                    &mut Tee::new(
                                        vec![
                                            tokio::fs::File::create("bob").await?,
                                            tokio::fs::File::create("bob2").await?,
                                            tokio::fs::File::create("bob3").await?,
                                        ],
                                        Some(stdout),
                                    ),
                                )
                                .await?;

                                Ok(ExitStatus::from_raw(0))
                            })
                        }),
                        vec!["bob2".to_string(), "bob3".to_string()],
                    ),
                    Job::new(JobType::External("cat".to_string()), Vec::new()),
                    Job::new(JobType::External("cat".to_string()), Vec::new()),
                    Job::new(JobType::External("cat".to_string()), Vec::new()),
                    Job::new(JobType::External("cat".to_string()), Vec::new()),
                ],
                stdout,
            )
            .spawn::<CollectingAccumulator>()?
            .await
        })
        .expect("Could not spawn");

    println!("Result: {:?}", ret);

    return;

    let mut input = String::with_capacity(1024);
    let mut stdin = BufReader::new(io::stdin());

    while stdin.read_line(&mut input).expect("Could not read") != 0 || !input.is_empty() {
        match parse(&input) {
            Ok(r) => {
                println!("{:?}", r);
                let taken = input.len() - r.0.len();
                input.drain(..taken);
            }
            Err(e) => {
                eprintln!("{:?}", e);
                return;
            }
        }
    }
}
